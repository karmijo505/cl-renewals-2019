<style>
    .w-100 { width: 100% }
    .w-60 { width: 60%; }
    .w-55 { width: 55%; }
    .w-45 { width: 45%; }
    .w-40 { width: 40%; }
    .f-left { float: left; }
    .f-right { float: right; }
    .t-left {text-align: left; }
    .t-right {text-align: right; }
    .t-center {text-align: center }
    .t-size10 {font-size: 1.10em; }
    .center {margin: 0 auto;}
    .bold {font-weight: bold;}
    .company-info {  padding: 45px; }
    .notification-info { padding: 45px 0;}
    .office-use-info {padding: 45px;}
    table th { border:1px solid #444; }
    fieldset {border: 1px solid #444; padding: 15px; width:50%;}

</style>

<div class="f-left w-55 t-left">
    <h4>Office of Superintendent of Insurance</h4>
    <div class="osi-address">
        <div>Company Licensing Bureau</div>
        <div>P.O. Box 1689</div>
        <div>Santa Fe, New Mexico 87504-1689</div>
        <div>1-505-827-4362</div>
    </div>
</div>

<div class="f-right w-45 t-left">
    <div class="f-left w-60 t-left t-size10">
    <div>Date of this Notice</div>
    <div>Taxable Year Affected</div>
    <div>Total Amount Due By</div>
    </div>
    <div class="f-right w-40 t-left t-size10">
        <div>{{ $noticeDate }}</div>
        <div>{{ $taxableYear }}</div>
        <div>{{ $dueDate }}</div>
    </div>
</div>

<div class="company-info f-left w-100 t-size10">
    <div>{{ $entityData['company_name'] }}</div>
    <div>{{ $entityData['mlg_address1'] }}</div>
    <span>{{ $entityData['mailing_city'] }}</span>
    <span>{{ $entityData['mailing_state'] }}</span>
    <span>{{ $entityData['mailing_zip'] }}</span>
</div>

<h3 class="t-center">NOTICE OF ANNUAL CONTINUATION OF CERTIFICATE OF AUTHORITY</h3>

<table width="100%">
    <tr>
        <th>Description</th>
        <th>Amount</th>
    </tr>
    @foreach ($entityData['lobs'] as $lob)
        <tr>
            <td class="t-left">{{  $lob['line_of_business'] }}</td>
            <td class="t-center">{{ money_format('%.2n', $lob['cost']) }}</td>
        </tr>
    @endforeach
    <tr>
        <td class="t-right">Total amount to be Remitted:</td>
        <td class="t-center bold">{{ money_format('%.2n', $totalAmt) }}</td>
    </tr>
</table>

<div class="notification-info w-100 t-left t-size10">
    In accordance with NMSA 1978 Section 59A-6-1 (R-1B) as amended, you are hereby notified the Motor Club's annual continuation of certifcate of authority fee of <span class="bold">${{ money_format('%.2n', $totalAmt) }}</span> is due on <span class="bold">{{ $dueDate }}</span>.
</div>

<div class="payment-info">
    <h3 class="t-center">Please Return Copy of This Notice With Your Payment</h3>
    <h3 class="t-center">Make Check Payable to: OSI</h3>
</div>

<div class="office-use-info w-100">
    <fieldset class="center">
        <legend>For Office Use Only</legend>
        Check No.<hr/>
        Amount Remitted $<hr />
    </fieldset>
</div>


<p style="page-break-before: always">

<div class="f-left w-55 t-left">
    <h4>Office of Superintendent of Insurance</h4>
    <div class="osi-address">
        <div>Company Licensing Bureau</div>
        <div>P.O. Box 1689</div>
        <div>Santa Fe, New Mexico 87504-1689</div>
        <div>1-505-827-4362</div>
    </div>
</div>

<div class="f-right w-45 t-left">
    <div class="f-left w-60 t-left t-size10">
        <div>Date of this Notice</div>
        <div>Taxable Year Affected</div>
        <div>Total Amount Due By</div>
    </div>
    <div class="f-right w-40 t-left t-size10">
        <div>{{ $noticeDate }}</div>
        <div>{{ $taxableYear }}</div>
        <div>{{ $dueDate }}</div>
    </div>
</div>

<div class="company-info f-left w-100 t-size10">
    <div>{{ $entityData['naic_cocode'] }}</div>
    <div>{{ $entityData['company_name'] }}</div>
    <div>{{ $entityData['mlg_address1'] }}</div>
    <span>{{ $entityData['mailing_city'] }}</span>
    <span>{{ $entityData['mailing_state'] }}</span>
    <span>{{ $entityData['mailing_zip'] }}</span>
</div>

<h3 class="t-center">REQUIREMENTS OF ANNUAL CONTINUATION OF CERTIFICATE OF AUTHORITY</h3>

<div class="notification-info w-100 t-left t-size10">
    <p> A financial statement, certified by a registerd or certified public accountant, or a financial statement signed by the accounting firm, within the previous twelve (12) months in accordance with Section 59A-50-5 (C).  Financial statement must be submitted by June 1, 2018.</p>
    <p> A statement indicating the amount of annual membership fees collected from residents of thte State of New Mexico for the period of January 1 through December 31, each year, due no later than May 1, 2018.  The amount of surety bond must be based upon the annual membership fees collected in New Mexico in accordance with Section 59A-50-4 (B-5).</p>
</div>