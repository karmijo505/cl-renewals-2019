@extends('layouts.baselayout')

@section('content')

    <div id="clrewnew-search" class="container">

        <h2>Search</h2>

        <div class="row">

                <div class="input-field col m4 s12">
                    <i class="material-icons prefix">filter_list</i>
                    <select id="filter-select">
                        <option value="companies">Insurance Companies</option>
                        <option value="fraternals">Fraternal Benefit Societies</option>
                        <option value="motorclubs">Motor Clubs</option>
                        <option value="prepaids">Pre-Paid Groups</option>
                        <option value="purchases">Purchasing Groups</option>
                        <option value="risks">Risk Retention Groups</option>
                    </select>
                </div>

                <div class="input-field col m5 s12">
                    <div id="scrollable-dropdown-menu">
                        <i class="material-icons prefix">search</i>
                        <div class="input-wrapper">
                            <input id="search-input"
                                   class="typeahead"
                                   placeholder="search by company or naic code"
                                   autocomplete="off">
                        </div>
                    </div>
                </div>

            <div class="col m3 s12" style="margin-top: .5em;">
                <a id="reset-button"
                   class="waves-effect waves-light btn reset grey darken-4"><i class="material-icons"
                                                                               style="vertical-align: middle">clear</i>Clear
                </a>
            </div>
        </div>

        <div class="row">
            <div class="col m11 s12">
                <div id="details-card" class="card hidden">
                    <div class="card-image">
                        <img src="images/companies.jpg">
                        <span class="card-title">Company Details</span>
                        <div class="c1">
                            <div class="d1"></div>
                        </div>
                        <a id="company-pdf" href="#" target="_blank"
                           class="btn-floating btn-large halfway-fab waves-effect waves-light red">
                            <i class="material-icons">receipt</i>
                        </a>
                    </div>
                    <div class="card-content">
                        <p id="js-company-details"></p>
                    </div>
                    <div class="card-tabs">
                        <ul class="tabs tabs-fixed-width">
                            <li class="tab">
                                <a class="active" href="#js-mailing-address">
                                    <i class="material-icons" style="vertical-align: middle">mail_outline</i>
                                    Mailing address
                                </a>
                            </li>
                            <li class="tab">
                                <a href="#js-lobs">
                                    <i class="material-icons" style="vertical-align: middle">business</i>
                                    Lines Of Business
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-content grey lighten-4">
                        <div id="js-mailing-address"></div>
                        <div id="js-lobs"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop