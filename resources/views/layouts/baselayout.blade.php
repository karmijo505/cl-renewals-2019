<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    {!! Html::style('css/app.css') !!}

</head>

<body>

<nav class="grey darken-4">
    <div class="nav-wrapper">
        <a href="/" class="brand-logo center">{{ config('app.name') }}</a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
        </ul>
    </div>
</nav>

@yield('content')


<!-- Scripts -->
{!! Html::script('js/all.js') !!}

</body>
</html>