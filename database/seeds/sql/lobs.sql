-- noinspection SqlNoDataSourceInspectionForFile

-- noinspection SqlDialectInspectionForFile

INSERT INTO lobs VALUES ('Accident and Health',1,200);
INSERT INTO lobs VALUES ('Casualty',2,200);
INSERT INTO lobs VALUES ('Property',3,200);
INSERT INTO lobs VALUES ('Life and Annuities',4,200);
INSERT INTO lobs VALUES ('Variable Life and Annuity',5,200);
INSERT INTO lobs VALUES ('Health Maintenance Organization',6,200);
INSERT INTO lobs VALUES ('Qualified Dental Plan',7,200);
INSERT INTO lobs VALUES ('Qualified Health Plan',8,200);
INSERT INTO lobs VALUES ('Non-Profit Health Care Plan',9,200);
INSERT INTO lobs VALUES ('Pre-Paid Dental',10,200);
INSERT INTO lobs VALUES ('Fraternal Benefit Society',11,0);
INSERT INTO lobs VALUES ('Annual Continuation of Registration Fee',12,200);
INSERT INTO lobs VALUES ('Liability Risk Retention Group',13,200);
INSERT INTO lobs VALUES ('Motor Club',14,100);
